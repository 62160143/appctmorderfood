package com.supakit.mobileappproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.supakit.mobileappproject.databinding.FragmentStatusCartBinding
import com.supakit.mobileappproject.viewModel.CoffeeViewModel

class StatusCart : Fragment() {
    private var _binding: FragmentStatusCartBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CoffeeViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStatusCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //show the next queue
        binding.currentQueue.text = viewModel.getNextQueue().toString()
        //Go to Home
        binding.btnGotoHome.setOnClickListener {
            val actionGotoHome = StatusCartDirections.actionStatusCartToHomeFragment()
            view.findNavController().navigate(actionGotoHome)
            // next Queue when go to home
            nextQueue()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun nextQueue() {
        viewModel.nextQueue()
    }


}