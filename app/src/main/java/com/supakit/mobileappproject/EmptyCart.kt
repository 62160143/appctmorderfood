package com.supakit.mobileappproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.supakit.mobileappproject.databinding.FragmentEmptyCartBinding

class EmptyCart : Fragment() {
    private var _binding: FragmentEmptyCartBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmptyCartBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Go to Home page
        binding?.btnGoHome?.setOnClickListener {
            val actionToGoHome = EmptyCartDirections.actionEmptyCartToHomeFragment()
            view.findNavController().navigate(actionToGoHome)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}