package com.supakit.mobileappproject.data

import com.supakit.mobileappproject.R
import com.supakit.mobileappproject.model.Coffee

class CoffeeData {
    fun getCoffeeData(): List<Coffee> {
        return listOf(
            Coffee(
                id = 1,
                name = "แบล็กคอฟฟี่(ร้อน)",
                price = 40.00,
                imageId = R.drawable.blackcoffeehot
            ),
            Coffee(
                id = 2,
                name = "แบล็กคอฟฟี่(เย็น)",
                price = 60.00,
                imageId = R.drawable.black_coffee_ice
            ),
            Coffee(
                id = 3,
                name = "เอสเปรสโซ(ร้อน)",
                price = 40.00,
                imageId = R.drawable.espreesohot
            ),
            Coffee(
                id = 4,
                name = "เอสเปรสโซ(เย็น)",
                price = 60.00,
                imageId = R.drawable.espressoice
            ),
            Coffee(
                id = 5,
                name = "เอสเปรสโซ(ปั่น)",
                price = 65.00,
                imageId = R.drawable.espressofrappe
            ),
            Coffee(
                id = 6,
                name = "อเมซอน(ร้อน)",
                price = 45.00,
                imageId = R.drawable.amazonhot
            ),
            Coffee(
                id = 7,
                name = "อเมซอน(เย็น)",
                price = 65.00,
                imageId = R.drawable.amazonice
            ),
            Coffee(
                id = 8,
                name = "อเมซอน(ปั่น)",
                price = 70.00,
                imageId = R.drawable.amazonfrappe
            ),
            Coffee(
                id = 9,
                name = "คาปูชิโน(ร้อน)",
                price = 45.00,
                imageId = R.drawable.cappuccinohot
            ),
            Coffee(
                id = 10,
                name = "คาปูชิโน(เย็น)",
                price = 60.00,
                imageId = R.drawable.cappuccinoice
            ),
            Coffee(
                id = 11,
                name = "คาปูชิโน(ปั่น)",
                price = 70.00,
                imageId = R.drawable.cappuccinofrappe
            ),
        )
    }
}