package com.supakit.mobileappproject.viewModel

import androidx.lifecycle.*
import com.supakit.mobileappproject.model.CoffeeCart


class CoffeeViewModel() : ViewModel() {

    private val _totalPrice = MutableLiveData<Double>()
    val totalPrice: LiveData<Double> = _totalPrice

    private val _currentAmount = MutableLiveData<Int>()
    val currentAmount: LiveData<Int> = _currentAmount

    val cartItems = mutableListOf<CoffeeCart>()

    var q = 1
    private var total = 0.0
    var amount = 0

    // Add Item to Cart
    fun addItem(item: CoffeeCart) {
        total += item.price
        cartItems.add(item)
        totalPrice()
        resetAmount()

    }

    fun remove(item: CoffeeCart) {
        total -= item.price
        cartItems.remove(item)
        totalPrice()
    }

    fun clear() {
        cartItems.clear()
    }

    private fun totalPrice() {
        _totalPrice.value = total
    }

    private fun resetAmount() {
        amount = 0
        _currentAmount.value = 0
    }

    fun nextQueue() {
        q += 1
    }

    fun getNextQueue(): Int {
        return q
    }

    fun increase() {
        amount++
        _currentAmount.value = amount
    }

    fun reduce() {
        if (amount <= 0) {
            _currentAmount.value = 0
        } else {
            amount--
            _currentAmount.value = amount
        }

    }

    fun init() {
        total = 0.0
        amount = 0
        _currentAmount.value = 0
    }
//    private fun getQueueEntry(queue:Int, total:String, status:String): Queue {
//        return Queue(
//            queue= queue,
//            total = total,
//            status = status
//        )
//    }
//    fun addQueue(queue:Int,total:String,status:String) {
//        val newItem = getQueueEntry(queue,total,status)
//        insertQueue(newItem)
//    }
//    private fun insertQueue(queue: Queue) {
//        viewModelScope.launch {
//            queueDao.insert(queue)
//        }
//    }


}
//class QueueViewModelFactory(private val queueDao: QueueDao) : ViewModelProvider.Factory {
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(CoffeeViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return CoffeeViewModel(queueDao) as T
//        }
//        throw IllegalArgumentException("Unknown ViewModel class")
//    }
//}

