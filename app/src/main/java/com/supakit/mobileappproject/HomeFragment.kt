package com.supakit.mobileappproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.supakit.mobileappproject.adapter.ItemAdapter
import com.supakit.mobileappproject.data.CoffeeData
import com.supakit.mobileappproject.databinding.FragmentHomeBinding
import com.supakit.mobileappproject.viewModel.CoffeeViewModel

class HomeFragment : Fragment() {
    private val viewModel: CoffeeViewModel by activityViewModels()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Show list adapter on recycleView
        val myDataset = CoffeeData().getCoffeeData()
        val recyclerView = binding.recyclerViewHome
        recyclerView.adapter = ItemAdapter(requireContext(), myDataset) {
            val action =
                HomeFragmentDirections.actionHomeFragmentToCoffeeDetailFragment(itemId = it.id)
            findNavController().navigate(action)
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // Show queue and size cart
        binding.apply {
            lengthCart.text = viewModel.cartItems.size.toString()
            countQueue.text = viewModel.getNextQueue().toString()

            // When click button cart to next page
            btnCart.setOnClickListener {
                //if size cart == 0 --> empty_cart
                if (viewModel.cartItems.size == 0) {
                    val actionToEmptyCart = HomeFragmentDirections.actionHomeFragmentToEmptyCart()
                    view.findNavController().navigate(actionToEmptyCart)
                }
                //if size cart > 0 --> cart list item
                else {
                    val actionToCartListItem =
                        HomeFragmentDirections.actionHomeFragmentToCartListItem()
                    view.findNavController().navigate(actionToCartListItem)
                }

            }
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}