package com.supakit.mobileappproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.supakit.mobileappproject.databinding.FragmentCoffeeDetailBinding
import com.supakit.mobileappproject.model.CoffeeCart
import com.supakit.mobileappproject.viewModel.CoffeeViewModel


class CoffeeDetailFragment : Fragment() {
    private var _binding: FragmentCoffeeDetailBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: CoffeeDetailFragmentArgs by navArgs()
    private lateinit var coffee: com.supakit.mobileappproject.model.Coffee
    val myDataset = com.supakit.mobileappproject.data.CoffeeData().getCoffeeData()
    var itemId = 0
    var sweetnessText = "หวานปกติ"
    private val viewModel: CoffeeViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCoffeeDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // retrieve id
        itemId = navigationArgs.itemId

        //find id match with dataset
        myDataset.forEach {
            if (it.id == itemId) {
                coffee = it
            }
        }

        //set text
        binding.apply {
            name.text = coffee.name
            price.text = coffee.price.toString()
            imgCoffee.setImageResource(coffee.imageId)
            AddToCart.setOnClickListener {
                if (viewModel.currentAmount.value == 0 || viewModel.currentAmount.value == null) {
                    Toast.makeText(requireContext(), "Please increase amount!", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    val actionConfirm =
                        CoffeeDetailFragmentDirections.actionCoffeeDetailFragmentToHomeFragment()
                    view.findNavController().navigate(actionConfirm)
                    addItemToCart()
                }
            }
        }
        selectSweetness()
        amountValue()

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun selectSweetness() {
        binding.apply {
            selectSweetness2.isChecked = true
            selectSweetness1.setOnClickListener {
                sweetnessText = "หวานมาก"
            }
            selectSweetness2.setOnClickListener {
                sweetnessText = "หวานปกติ"
            }
            selectSweetness3.setOnClickListener {
                sweetnessText = "หวาน 25%"
            }
            selectSweetness4.setOnClickListener {
                sweetnessText = "หวาน 0%"
            }
        }
    }

    private fun amountValue() {
        binding.apply {
            add.setOnClickListener {
                viewModel.increase()
            }

            remove.setOnClickListener {
                viewModel.reduce()
            }
            viewModel.currentAmount.observe(viewLifecycleOwner) { newAmount ->
                binding.countCoffee.text = newAmount.toString()
            }
        }
    }

    private fun addItemToCart() {
        var amount = viewModel.currentAmount.value!!.toInt()
        viewModel.addItem(
            CoffeeCart(
                coffee.name,
                coffee.price * amount,
                coffee.imageId,
                sweetnessText,
                amount
            )
        )
    }

}