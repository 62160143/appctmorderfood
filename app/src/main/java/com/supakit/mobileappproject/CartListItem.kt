package com.supakit.mobileappproject

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.supakit.mobileappproject.adapter.CartAdapter
import com.supakit.mobileappproject.databinding.FragmentCartListItemBinding
import com.supakit.mobileappproject.viewModel.CoffeeViewModel

class CartListItem : Fragment() {
    private var _binding: FragmentCartListItemBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CoffeeViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCartListItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // show list cart adapter on RecyclerviewCart
        val myDataset = viewModel.cartItems
        val recyclerView = binding.recycleAddToCart
        recyclerView.adapter = CartAdapter(requireContext(), myDataset) {
            viewModel.remove(it)
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // Click confirm

        // LiveData
        viewModel.totalPrice.observe(this.viewLifecycleOwner) { newToTalPrice ->
            binding.totalPrice.text = newToTalPrice.toString()
            if (newToTalPrice == 0.00) {
                findNavController().navigate(R.id.action_cart_list_item_to_empty_cart)
            }
            binding.confirmCart.setOnClickListener {
                // init
                init()
                //clear cart
                clearCart()
                // confirm
                val actionConfirm = CartListItemDirections.actionCartListItemToStatusCart(
                    queue = viewModel.getNextQueue(),
                    total = newToTalPrice.toString(),
                    status = "กำลังดำเนินการ"
                )
                view.findNavController().navigate(actionConfirm)
            }
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun clearCart() {
        viewModel.clear()
    }

    private fun init() {
        viewModel.init()
    }

}