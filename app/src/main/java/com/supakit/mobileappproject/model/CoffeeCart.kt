package com.supakit.mobileappproject.model

import androidx.annotation.DrawableRes

data class CoffeeCart(
    val name: String,
    val price: Double,
    @DrawableRes val imageId: Int,
    val sweetness: String,
    val amount: Int
)