package com.supakit.mobileappproject.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.supakit.mobileappproject.R
import com.supakit.mobileappproject.model.Coffee
import com.supakit.mobileappproject.model.CoffeeCart
import com.supakit.mobileappproject.viewModel.CoffeeViewModel

class CartAdapter(
    private val context: Context,
    private val dataset: MutableList<CoffeeCart>,
    private val onItemClicked: (item: CoffeeCart) -> Unit
) : RecyclerView.Adapter<CartAdapter.CartItemViewHolder>() {
    class CartItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.rpt_name)
        val imageView: ImageView = view.findViewById(R.id.coffeeImageCart)
        val price: TextView = view.findViewById(R.id.rpt_price)
        val sweetness: TextView = view.findViewById(R.id.rpt_sweetness)
        val amount: TextView = view.findViewById(R.id.rpt_amount)
        val item: MaterialCardView = view.findViewById(R.id.adapterCart)
        val removeItem: ImageView = view.findViewById(R.id.removeItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemViewHolder {
        // create a new view
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_cart_list_item, parent, false)

        return CartItemViewHolder(adapterLayout)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: CartItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.name.text = item.name
        holder.imageView.setImageResource(item.imageId)
        holder.price.text = item.price.toString()
        holder.sweetness.text = item.sweetness
        holder.amount.text = item.amount.toString()
        holder.removeItem.setOnClickListener {
            onItemClicked(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = dataset.size


}