package com.supakit.mobileappproject.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.supakit.mobileappproject.R
import com.supakit.mobileappproject.model.Coffee

class ItemAdapter(
    private val context: Context,
    private val dataset: List<Coffee>,
    private val onItemClicked: (Coffee) -> Unit
) : RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.name)
        val imageView: ImageView = view.findViewById(R.id.coffeeImage)
        val price: TextView = view.findViewById(R.id.price)
        val item: MaterialCardView = view.findViewById(R.id.adapterHome)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        // create a new view
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_coffee_list_item, parent, false)

        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.name.text = item.name
        holder.imageView.setImageResource(item.imageId)
        holder.price.text = item.price.toString()
        holder.item.setOnClickListener {
            onItemClicked(item)
        }
    }

    override fun getItemCount() = dataset.size
}